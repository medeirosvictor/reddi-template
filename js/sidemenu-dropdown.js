$('.menu-item-parent').click(function() {
    $input = $( this );
    $target = $('#'+$input.attr('data-toggle'));
    $target.slideToggle();
    $input.find('i:last-child').toggleClass('glyphicon-menu-right glyphicon-menu-down'); 
});
